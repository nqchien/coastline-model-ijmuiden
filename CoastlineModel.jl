# CoastlineModel.jl: simulating coastline change due to sea waves

using DelimitedFiles
using Interpolations

function interp1(xpt, ypt, x; method="linear", extrapvalue=nothing)

    if extrapvalue == nothing
        y = zeros(x)
        idx = trues(x)
    else
        y = extrapvalue*ones(x)
        idx = (x .>= xpt[1]) .& (x .<= xpt[end])
    end
    
    if method == "linear"
        intf = interpolate((xpt,), ypt, Gridded(Linear()))
        y[idx] = intf[x[idx]]

    elseif method == "cubic"
        itp = interpolate(ypt, BSpline(Cubic(Natural())), OnGrid())
        intf = scale(itp, xpt)
        y[idx] = [intf[xi] for xi in x[idx]]
    end
    
    return y
end


function integrate(x1, x2, xin, yin)
    dx = xin[2] - xin[1]
    x = [xin[1] - dx/2  :  dx  :  xin[end] + dx/2]
    y = [0  yin  0]
    f[1] = 0
    for i = 2 : length(x)
        f[i] = f[i-1] + y[i]
    end
    if x1 < x[1]
        f1 = f[1]
    else
        f1 = interp1(x, f, x1)
    end
    if x2 > x[end]
        f2 = f[end]
    else
        f2 = interp1(x, f, x2)
    end
    return f2 - f1
end


# Input parameters
ns = 1000            # Number of grid cells
d = 4                # Height of active profile
dt = 0.1             # Time step (yr)
nt = 500             # Number of time steps
interval = 5         # Output interval

# Initial coastline
xyi = readdlm("data/2004-edit.txt")
xi = xyi[:,1]
yi = xyi[:,2]

# Reference coastline
xy = readdlm("data/2004-edit.txt")
xref = xy[:,1]
yref = xy[:,2]

# Present coastline
xyp = readdlm("data/2011-edit.txt")
xp = xyp[:,1]
yp = xyp[:,2]

# Compute distance along reference coastline
dist[1] = 0
for i = 2 : length(xref)
    dist[i] = dist[i-1] + sqrt((xref[i] - xref[i-1])^2 + (yref[i] - yref[i-1])^2)
end

# Create computational grid
ds = (dist[end] - dist[1])/(ns-1)  # Alongshore stepsize
s = 0 : ds : dist[end]             # Alongshore distance
xr = spline(dist, xref, s)         # x of coastline grid points
yr = spline(dist, yref, s)         # y of coastline grid points

#  Compute locations of initial coastline
[si, ni] = locate(s, xr, yr, xi, yi)
n = interp1(si, ni, s; "linear", "extrap")    # Cross-shore coastline change
for i in 2 : ns-1
    ds2[i] = sqrt((xr[i+1] - xr[i-1])^2 + (yr[i+1] - yr[i-1])^2)
    dy =  n[i] * (xr[i+1] - xr[i-1]) / ds2[i]
    dx = -n[i] * (yr[i+1] - yr[i-1]) / ds2[i]
    x[i] = xr[i] + dx
    y[i] = yr[i] + dy
    if i==2
        x[1] = xr[1] + dx
        y[1] = yr[1] + dy
    end
    if i == ns-1
        x[ns] = xr[ns] + dx
        y[ns] = yr[ns] + dy
    end
end

nS = 0.5 * (n[1:ns-1] + n[2:ns])      # n of transport points
sS = 0.5 * (s[1:ns-1] + s[2:ns])      # s of transport points
xS = 0.5 * (x[1:ns-1] + x[2:ns])      # x of transport points
yS = 0.5 * (y[1:ns-1] + y[2:ns])      # y of transport points

# Plot initial coastline
# plot(xr,yr,'.-k',xi,yi,'.',x,y,'r.-')

# Load groin locations / optional

# S-phi curves

load([workpath, "S_phi.mat"], "phic", "wavedir", "Snet", "Splus", "Smin")
ww = readdlm("data/WaveWindows.txt")
[sww, nww] = locate(s, x, y, ww[:,1] ,ww[:,2])
angle1 = interp1(sww, ww[:,3], sS; "linear", "extrap")
angle2 = interp1(sww, ww[:,4], sS; "linear", "extrap")

# Compute local S-phi curves
for i = 1 : ns-1
    for iphi = 1:length(phic)
        Sploc[i,phi] = integrate(angle1[i], angle2[i], wavedir, Splus[iphi,:])
        Smloc[i,phi] = integrate(angle1[i], angle2[i], wavedir, Smin[iphi,:])
    end
end

# Start time loop
for it = 1 : nt
    # Compute coast angles (direction of shore normal, nautical convention)
    for i = 1 : ns-1
        phi[i] = (2*pi-atan2(y[i+1] - y[i], x[i+1] - x[i])) * 180 / π
        if phi[i] > 360
            phi[i] = phi[i] - 360
        end
    end
    # Compute transport from sum of Splus(phi) and Smin(phi)
    for i = 1 : ns-1
        Sp[i] = interp1(phic, Sploc[i,:], phi[i])
        Sm[i] = interp1(phic, Smloc[i,:], phi[i])
    end
    if groins
        # Apply influence factors due to groins
        # Locate gridpoints at groin locations
        [sgroin, ngroin_end] = locate(s, x, y, xgroin[:,1], ygroin[:,1])
        ngroin_tip = ngroin_end + length_groin'
        ngroin_coast = interp1(s, n, sgroin)
        igroin = round(sgroin/ds) + 1
        dist_active = -500
        dist_end = ngroin_coast - ngroin_end
        dist_tip = ngroin_coast - ngroin_tip
        for ig = 1:length(sgroin)
            reducfac[ig] = interp1(dist_cross, ScumRel, dist_tip[ig]) ...
                - interp1(dist_cross, ScumRel, dist_active) ...
                + 1 - interp1(dist_cross, ScumRel, dist_end[ig])
            Sp[igroin[ig]] = Sp[igroin[ig]] * reducfac[ig]
            Sm[igroin[ig]-1] = Sm[igroin[ig]-1] * reducfac[ig]
        end
    end
    S = Sp + Sm
    # Stop if bad things happen
    if sum(isnan.(S)) > 0
        break
    end
    # Update coastline
    for i = 2 : ns-1
        dn = -dt / d * (S[i] - S[i-1]) / ds
        n[i] = n[i] + dn
        dy =  n[i] * (xr[i+1] - xr[i-1]) / ds2[i]
        dx = -n[i] * (yr[i+1] - yr[i-1]) / ds2[i]
        x[i] = xr[i] + dx
        y[i] = yr[i] + dy
        if i == 2
            # First boundary condition: constant transport gradient
            x[1] = xr[1] + dx
            y[1] = yr[1] + dy
            n[1] =  n[1] + dn
        end
        if i == ns-1
            # Second boundary condition: constant transport gradient
            x[ns] = xr[ns] + dx
            y[ns] = yr[ns] + dy
            n[ns] =  n[ns] + dn
        end
    end
    # Update yS and nS
    yS = 0.5 * (y[1:ns-1] + y[2:ns])
    nS = 0.5 * (n[1:ns-1] + n[2:ns])
    # Plot results
    if it % interval == 0
        # plot
    end
end
