# Mô hình biến động đường bờ 

## Áp dụng 

Việc mở rộng cảng IJmuiden, Hà Lan, đòi hỏi phải đánh giá được tác động của các đập phá sóng lên đường bờ vùng lân cận. 
Hình dạng đường bờ quan trắc vào năm 1967 và 2007 được trích từ cơ sở dữ liệu [Jarkus](https://publicwiki.deltares.nl/display/OET/Dataset+documentation+JarKus). Vị trí đường bờ dưới dạng những cặp toạ độ (X, Y) được lưu trong biến `ijmuiden`. Chế độ sóng trong khoảng thời gian này được tổng kết bằng số liệu lưu vào biến `climate`. Các cột trong dữ liệu `climate` này gồm có: tần suất _p_, chiều cao sóng _H<sub>m0</sub>_, chu kì sóng _T<sub>p</sub>_ và góc hướng sóng &alpha;<sub>0</sub> (dir) ở vùng nước sâu.

## Cơ sở lý thuyết 
Trong mô hình số, đường bờ được rời rạc hoá thành các đoạn cong, mỗi đoạn có độ dài &Delta;s. Kết quả tính toán bao gồm: phân bố theo phương ngang bờ các đặc trưng sóng và lượng vận chuyển bùn cát; phân bố dọc bờ vận chuyển bùn cát của các đoạn; diễn biến đường bờ theo thời gian.

## Chương trình phần mềm 
Được viết bằng ngôn ngữ lập trình Lua, giúp cho học viên dễ dàng xem và sửa đổi mã lệnh. Toàn bộ chương trình đọc trong 1 file. 

## Tài liệu tham khảo
Roelvink D, Reniers A (2011). A guide to modeling coastal morphology. _World scientific_.

